define({ "api": [
  {
    "type": "get",
    "url": "/location/:id",
    "title": "Request Location information",
    "name": "GetLocation",
    "group": "Location",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Location unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>of the Location.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "latitud",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "longitud",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "DateTime",
            "optional": false,
            "field": "fecha_registro",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "distancia",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "velocidad",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tipo",
            "description": "<p>of the Location</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "vehiculo",
            "description": "<p>información del vehículo.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "gps/routes/location.js",
    "groupTitle": "Location"
  },
  {
    "type": "get",
    "url": "/location/",
    "title": "Request Location information",
    "name": "GetLocations",
    "group": "Location",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "Location",
            "description": "<p>información de la ubicación.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "gps/routes/location.js",
    "groupTitle": "Location"
  },
  {
    "type": "get",
    "url": "/vehicle/:id/location",
    "title": "Request Vehicles Location information with filters",
    "name": "GetLocation",
    "group": "Vehicle",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Plate or Economic No.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "from",
            "description": "<p>Date filter (optional).</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "to",
            "description": "<p>Date filter (optional).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "vehiculo",
            "description": "<p>información del vehículo.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "gps/routes/vehicle.js",
    "groupTitle": "Vehicle"
  },
  {
    "type": "get",
    "url": "/vehicle/:id",
    "title": "Request Vehicle information",
    "name": "GetVehicle",
    "group": "Vehicle",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Plate or Economic No.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "vehiculo",
            "description": "<p>información del vehículo.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "gps/routes/vehicle.js",
    "groupTitle": "Vehicle"
  },
  {
    "type": "get",
    "url": "/vehicle/",
    "title": "Request Vehicles information",
    "name": "GetVehicles",
    "group": "Vehicle",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "vehiculo",
            "description": "<p>información del vehículo.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "gps/routes/vehicle.js",
    "groupTitle": "Vehicle"
  }
] });
