﻿var express = require('express');
var passport = require('passport');
var router = express.Router();
var Promise = require("bluebird");
var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var rutaDto = require('../dto/rutaDTO');
var userDto = require('../dto/UserDTO');

/*
* Response user information
*/
router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    //var model = {
    //    "id": req.user.id,
    //    "username": req.user.username,
    //    "phone": req.user.phone,
    //    "createdAt": req.user.createdAt,
    //    "updatedAt": req.user.updatedAt,
    //};
    //console.log(model);
    //model.creditCard = {};
 

    models.Cliente_web.findOne({
        //attributes: vehicleDto,
        attributes: userDto,
        where: { Cliente_ID: req.user.Cliente_ID }
    }).then(function (resp) {
        console.log(resp);
        res.json(resp);

    }).catch(function (err) {
        res.statusCode = 400;
       
        res.json(err);
    });

    //models.Vehicle.findAll( {
    //    include: [{model: models.GpsRoutes}],
    //    where: { Cliente_ID: 2 }
    //}).then(function (resp) {
    //    console.log(resp);
    //    res.json(resp);

    //}).catch(function (err) {
    //    res.statusCode = 400;
    //    
    //    res.json(err);
    //});

}
);

module.exports = router;