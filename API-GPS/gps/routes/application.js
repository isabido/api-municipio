﻿var express = require('express');
var passport = require('passport');
var router = express.Router();
var Promise = require("bluebird");
var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var config = require('../config/config.json');
var crypto = require('crypto');
var utils = require('../utils/utils');
var applicationDto = require('../dto/applicationDTO');
var applicationQuery = require('../queries/application/applicationQuery');
var _ = require('underscore');

/*
* Create an application to connect with GPS API
*/
router.post('/', function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    //var model = {
    //    "id": req.user.id,
    //    "username": req.user.username,
    //    "phone": req.user.phone,
    //    "createdAt": req.user.createdAt,
    //    "updatedAt": req.user.updatedAt,
    //};
    //console.log(model);
    //model.creditCard = {};
    if (req.body != null) {
        req.body.application_secret = utils.createSecret();
        console.log("secret" + req.body.application_secret);
        
        applicationDto.mappModel(req.body);
        var _addApplication = applicationQuery.addApplication(req.body.application_name, req.body.application_id, req.body.application_secret);
        
        _addApplication.then(function (resp) {
            if (_.isEmpty(resp) || resp === null) {
                res.json(error.getNotRows('No se agregaron los datos'));
            } else {
                res.json(applicationDto.getMapModel());
            }
            
        })
        .catch(function (err) {
            
            res.statusCode = 400;
            console.log(err);
            res.json({error: err.message
        });
        });
        
    }
    
    //models.Application.findOne().then(function (resp) {
    //    console.log(resp);
    //    res.json(applicationDto);

    //}).catch(function (err) {
    //    res.statusCode = 400;
    //    var errors = errorsHelpers.DataBaseError(err);
    //    res.json(errors);
    //});

}
);

module.exports = router;