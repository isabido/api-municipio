﻿"use strict"
var express = require('express');
var passport = require('passport');
var router = express.Router();
var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var rutaDto = require('../dto/rutaDTO');
var error = require('../Errors/general');
var locationQuery = require('../queries/location/locationQuery');
var config = require('../config/config.json');
var check = require('validator');
var moment = require('moment');
/**
 * @api {get} /location/:id Request Location information
 * @apiName GetLocation
 * @apiGroup Location
 *
 * @apiParam {Number} id Location unique ID.
 *
 * @apiSuccess {Number} id of the Location.
 * @apiSuccess {String} latitud of the Location
 * @apiSuccess {String} longitud of the Location
 * @apiSuccess {DateTime} fecha_registro of the Location
 * @apiSuccess {Double} distancia of the Location
 * @apiSuccess {Double} velocidad of the Location
 * @apiSuccess {Number} tipo of the Location
 * @apiSuccess {Object} vehiculo información del vehículo.
 */
router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getLocation = locationQuery.findLocationById(req.params.id, req.user);
    _getLocation.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);


/**
 * @api {get} /location/ Request Location information
 * @apiName GetLocations
 * @apiGroup Location
 *
 * @apiSuccess {Object[]} Location información de la ubicación.
 */
router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var offset = (req.query.offset || 1) - 1;
    offset = offset * config.response_limit;
    if (req.query.offset && req.query.offset >= 0 && !req.query.from && !req.query.to) {
        
        var _getLocations = locationQuery.findLocationsLimiting(offset, config.response_limit, req.user);
        _getLocations.then(function (resp) {
            var locations = {
                count: resp.count,
                from: offset + 1,
                to: offset + resp.rows.length,
                rows: resp.rows.length,
                locations: resp.rows
            };
            if (resp.rows.length > 0) {
                res.json(locations);
            } else {
                var msg = {
                    message: "No hay mas datos"
                };
                res.json(msg);
            }
            
        })
            .catch(function (err) {
            res.statusCode = 400;
            
            res.json(err);
        });
        
    } else if (req.query.from && req.query.to && req.query.offset && req.query.offset >= 0) {
        var formats = [
            moment.ISO_8601,
            "YYYY-MM-DD"
        ];
        if (!moment(req.query.from, formats, 'en', true).isValid() && !moment(req.query.to, formats, 'en', true).isValid()) {
            throw new Error("From y To deben ser fechas validas (YYYY-MM-DD).");
        }
        //if (!check.isDate(req.query.from, { format: 'YYYY-MM-DD' }))
        //    throw new Error("From Must be a valid date");
        var _getLocations = locationQuery.getLocationFrom(req.query.from, req.query.to, req.query.offset, config.response_limit, req.user);
        _getLocations.then(function (resp) {
            var locations = {
                count: resp.count,
                from: offset + 1,
                to: offset + resp.rows.length,
                rows: resp.rows.length,
                locations: resp.rows
            };
            if (resp.rows.length > 0) {
                res.json(locations);
            } else {
                var msg = {
                    message: "No hay mas datos"
                };
                res.json(msg);
            }
        })
            .catch(function (err) {
            res.statusCode = 400;
            res.json(err);
        });
        
    }
    else {
        res.statusCode = 400;
        var err = {
            message:"You must send offset filter, example: locations/?offset=1"
        };
        res.json(err);
    }
}
);


router.get('/date', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    console.log("en date");
    if (req.query.from) {
        if (!check.isDate(req.query.from, { format: 'YYYY-MM-DD' }))
            throw new Error("From Must be a valid date");
        var _getLocations = locationQuery.getLocationFrom(req.query.from, req.user);
        _getLocations.then(function (resp) {
            res.json(msg);            
        })
            .catch(function (err) {
            res.statusCode = 400;
            
            res.json(err);
        });
        
    } else {
        res.statusCode = 400;
        var err = {
            message: "You must send from filter, example: locations/date?from=2016-02-15"
        };
        res.json(err);
    }
    
}
);

module.exports = router;