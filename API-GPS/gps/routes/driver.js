﻿var express = require('express');
var passport = require('passport');
var router = express.Router();
var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var driverDto = require('../dto/driverDTO');
var error = require('../Errors/general');
var driverQuerys = require('../queries/driver/driverQuery');
var _ = require('underscore');
var check = require('validator');
var moment = require('moment');
/*
* Response user information
*/
router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.

    var _getDriver = driverQuerys.findDriverById(req.params.id, req.user);
    _getDriver.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);


router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    if (req.query.from) {
        var formats = [
            moment.ISO_8601,
            "YYYY-MM-DD"
        ];
        if (!moment(req.query.from, formats, 'en', true).isValid()) {
            throw new Error("From debe ser una fecha valida en el formato (YYYY-MM-DD).");
        }
        var _getAllDrivers = driverQuerys.searchDriversFrom(req.query.from, req.user);
    } else {
        var _getAllDrivers = driverQuerys.findAllDrivers(req.user);
        
    }

    _getAllDrivers.then(function (resp) {
        
        if (_.isEmpty(resp)) {
            res.json(error.getNotRows('No se encontraron Choferes'));
        } else {
            res.json(resp);
        }
        
    })
    .catch(function (err) {
        res.statusCode = 400;
        console.log("prueba");
        res.json(err);
    });
    
    

}
);

module.exports = router;