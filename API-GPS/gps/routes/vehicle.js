﻿var express = require('express');
var passport = require('passport');
var router = express.Router();

var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var rutaDto = require('../dto/rutaDTO');
var error = require('../Errors/general');
var vehicleQuery = require('../queries/vehicle/vehicleQuery');
var check = require('validator');
var config = require('../config/config.json');
var moment = require('moment');
/**
 * @api {get} /vehicle/:id Request Vehicle information
 * @apiName GetVehicle
 * @apiGroup Vehicle
 *
 * @apiParam {String} id Plate or Economic No.
 *
 * @apiSuccess {Object} vehiculo información del vehículo.
 */
router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getVehicle = vehicleQuery.findVehicleById(req.params.id, req.user);
    _getVehicle.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);

/**
 * @api {get} /vehicle/ Request Vehicles information
 * @apiName GetVehicles
 * @apiGroup Vehicle
 *
 * @apiSuccess {Object} vehiculo información del vehículo.
 */
router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getAllVehicle = vehicleQuery.findAllVehicle(req.user);
    _getAllVehicle.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);

/**
 * @api {get} /vehicle/:id/location Request Vehicles Location information with filters
 * @apiName GetLocation
 * @apiGroup Vehicle
 *
 * @apiParam {String} id Plate or Economic No.
 * @apiParam {Date} from Date filter (optional).
 * @apiParam {Date} to Date filter (optional).
 *
 * @apiSuccess {Object} vehiculo información del vehículo.
 */
router.get('/:id/location?', passport.authenticate('bearer', { session: false }), function (req, res) {
    var formats = [
        moment.ISO_8601,
        "YYYY-MM-DD"
    ];
    var offset = (parseInt(req.query.offset) || 1) - 1;
    offset = offset * config.response_limit;
    
    if (req.query.from && !req.query.to) {
        if (!moment(req.query.from, formats, 'en', true).isValid()) {
            throw new Error("From debe ser una fechas valida con el formato (YYYY-MM-DD).");
        }

        var _getAllVehicleLocation = vehicleQuery.getVehicleLocationFrom(req.params.id, req.query.from, offset , config.response_limit, req.user);
    }
    else if (req.query.from && req.query.to) {

        if (!moment(req.query.from, formats, 'en', true).isValid() && !moment(req.query.to, formats, 'en', true).isValid()) {
            throw new Error("From y To deben ser fechas validas en el formato (YYYY-MM-DD).");
        }

        var _getAllVehicleLocation = vehicleQuery.getVehicleLocationFromTo(req.params.id, req.query.from, req.query.to, offset , config.response_limit, req.user);
    } else if ((!req.query.from && !req.query.to)) {

        var _getAllVehicleLocation = vehicleQuery.getLastLocation2(req.params.id, req.user);
    }
    if (_getAllVehicleLocation !== "undefined" || _getAllVehicleLocation !== null) {
        _getAllVehicleLocation.then(function (resp) {

            if (resp !== null && resp !== 'undefined' && !resp.message) {
                
                var idArray = [];
                console.log(resp.count && resp.rows)
                if ((!req.query.from && !req.query.to)) {
                    
                    var rows = JSON.stringify(resp);
                    var jsonRows = JSON.parse(rows);
                    
                    for (var i in jsonRows) {
                        idArray.push(jsonRows[i].id);  
                    }
                    var update = vehicleQuery.updateLocationEnviado(idArray);
                } else if (resp.count && resp.rows) {
                    resp = {
                        count: resp.count,
                        from: offset + 1,
                        to: offset + resp.rows.length,
                        rows: resp.rows.length,
                        locations: resp.rows
                    };
                }
            }
            res.json(resp);
        })
    .catch(function (err) {
            res.statusCode = 400;
            res.json(err);
        });
    } else {
        resp = { message: "No se encontraron datos" };
        res.statusCode = 400;
        res.json(resp);
    }

}
);

router.get('/:id/routes', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getAllRoutes = vehicleQuery.getVehicleRoutes(parseInt(req.params.id), req.user);
    _getAllRoutes.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);


router.get('/:id/drivers', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getAllDrivers = vehicleQuery.getDriversForVehicle(parseInt(req.params.id), req.user);
    _getAllDrivers.then(function (resp) {
        res.json(resp);
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);

module.exports = router;