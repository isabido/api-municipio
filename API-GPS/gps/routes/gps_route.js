﻿var express = require('express');
var passport = require('passport');
var router = express.Router();
var Promise = require("bluebird");
var proxy = require('express-http-proxy');
var libs = '../';
var log = require(libs + 'log')(module);
var models = require(libs + 'models');
var rutaDto = require('../dto/rutaDTO');
var error = require('../Errors/general');
var routesQuery = require('../queries/gps_route/gps_routeQuery');
var _ = require('underscore');

/*
* Response user information
*/
router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getRoute = routesQuery.findRouteById(req.params.id, req.user);
    _getRoute.then(function (resp) {
        if (_.isEmpty(resp)) {
            res.json(error.getNotRows('No se encontraron Rutas'));
        } else {
            res.json(resp);
        }
                    
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);

router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    var _getAllRoutes = routesQuery.findAllRoutes(req.user);
    _getAllRoutes.then(function (resp) {
        if (_.isEmpty(resp)) {
            res.json(error.getNotRows('No se encontraron Rutas'));
        } else {
            res.json(resp);
        }           
        
    })
    .catch(function (err) {
        res.statusCode = 400;
        
        res.json(err);
    });
    

}
);

module.exports = router;