﻿"use strict";
var models = require('../../models');
var rutaDto = require('../../dto/rutaDTO');
var error = require('../../Errors/general');
var vehicleDto = require('../../dto/vehicleDTO');
var _ = require('underscore');
var exports = module.exports = {};

exports.findRouteById = function (id, client) {
    return models.GpsRoutes.findOne({
        where: {
            id_ruta: id,
            reg_status: 'A'      
        },
        attributes: rutaDto
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontró la ruta');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.findAllRoutes = function (client) {
    return models.GpsRoutes.findAll({
        attributes: rutaDto,
        where: {
            reg_Status: 'A'
        } 
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron Rutas');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};