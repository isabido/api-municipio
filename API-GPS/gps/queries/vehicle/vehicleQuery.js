﻿"use strict";
var models = require('../../models');
var rutaDto = require('../../dto/rutaDTO');
var error = require('../../Errors/general');
var vehicleDto = require('../../dto/vehicleDTO');
var locationDto = require('../../dto/locationDTO');
var driverDto = require('../../dto/driverDTO');

var exports = module.exports = {};

exports.findVehicleById = function (id, client) {
    return models.Vehicle.findOne({
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
            
        },
        attributes: vehicleDto
    }).then(function (resp) {
        console.log(resp);
        if (resp === null) {
            return error.getNotRows('No se encontró el vehículo');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.findAllVehicle = function (client) {
    return models.Vehicle.findAll({
        attributes: vehicleDto,
        where: {
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron los vehículos');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};


exports.getVehicleLocation = function (id, client) {
    var today = new Date();
    today.setUTCHours(0, 0, 0, 0);
    var tomorrow = new Date(today);
    tomorrow.setUTCHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1); 
    return models.Vehicle.findOne({
        include: [{
                model: models.Location,
                attributes: locationDto,
                where: {
                    Reg_Status: 'A'
                }
                
            }],
        attributes: vehicleDto,
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'

            
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getVehicleLocationFrom = function (id, from, offset, limit, client) {
    console.log("getvehiclefrom")
    var today = new Date(from);
    today.setUTCHours(0, 0, 0, 0);
    return models.Location.findAndCountAll({
        include: [{
                model: models.Vehicle,
                attributes: [],
                where: {
                    $or: [
                        {
                            Vehiculo_Placas: id
                        }, 
                        {
                            Vehiculo_ID: id
                        }],
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                
                }
            }],
        attributes: locationDto,
        where: {
            Fecha: {
                $gte: today,
            },
            Reg_Status: 'A'
        },
        limit: limit,
        offset: parseInt(offset)
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getVehicleLocationFromTo = function (id, from, to, offset, limit, client) {
    var today = new Date(from);
    today.setUTCHours(0, 0, 0, 0);
//    var str1 = today.getUTCFullYear() + "-" +
//  ("0" + (today.getUTCMonth() + 1)).slice(-2) + "-" +
//  ("0" + today.getUTCDate()).slice(-2) + " " +
//  ("0" + today.getUTCHours()).slice(-2) + ":" +
//  ("0" + today.getUTCMinutes()).slice(-2) + ":" +
//  ("0" + today.getUTCSeconds()).slice(-2);

    var tomorrow = new Date(to);
    tomorrow.setUTCHours(23, 59, 59, 0);
//    var str2 = tomorrow.getUTCFullYear() + "-" +
//  ("0" + (tomorrow.getUTCMonth() + 1)).slice(-2) + "-" +
//  ("0" + tomorrow.getUTCDate()).slice(-2) + " " +
//  ("0" + tomorrow.getUTCHours()).slice(-2) + ":" +
//  ("0" + tomorrow.getUTCMinutes()).slice(-2) + ":" +
//  ("0" + tomorrow.getUTCSeconds()).slice(-2);
    
//    var q = "SELECT `Vehicle`.*, `Locations`.`idDatosLongitud` AS `Locations.idDatosLongitud`, `Locations`.`idDatosLongitud` AS `Locations.id`, `Locations`.`LongitudConvertida` AS `Locations.longitud`, `Locations`.`LatitudConvertida` " +
//    "AS `Locations.latitud`, `Locations`.`Fecha` AS `Locations.fecha_registro`, `Locations`.`Distancia` AS `Locations.distancia`, `Locations`.`Velocidad` AS `Locations.velocidad`, `Locations`.`TIPO` AS ` Locations.tipo` FROM(SELECT`Vehicle`.`Vehiculo_ID`, `Vehicle`.`Vehiculo_ID` AS `id`, `Vehicle`.`Vehiculo_Numero` AS " +
//    "`numero`, `Vehicle`.`Vehiculo_Marca` AS `marca`, `Vehicle`.`Vehiculo_Modelo` AS `modelo`, `Vehicle`.`Vehiculo_Placas` AS `placa`, `Vehicle`.`Reg_Timestamp` AS " +
//    "`fecha_registro`, `Vehicle`.`Vehiculo_kms` AS `kms`, `Vehicle`.`Descripcion` AS `descripcion`, `Vehicle`.`Num_PolizaSeguro` AS `numero_poliza`, `Vehicle`.`Vencimiento_Poliza` AS " +
//    "`vencimiento_poliza`, `Vehicle`.`Poliza_Seguro` AS `poliza_seguro`, `Vehicle`.`Tarjeta_Circulacion` AS `tarjeta_circulacion`, `Vehicle`.`aseguradora` AS " +
//    "`aseguradora`, `Vehicle`.`latitud_` AS `latitud`, `Vehicle`.`longitud_` " +
//    "AS `longitud`, `Vehicle`.`IdDispositivo` AS `identificador_gps`, `Vehicle`.`Vehiculo_NoEconomico` AS `vehiculo_NoEconomico` FROM `vehiculo` AS `Vehicle` WHERE(" +
//"`Vehicle`.`Vehiculo_Placas` = '" + id +"' OR `Vehicle`.`Vehiculo_NoEconomico` = '"+id+"') AND `Vehicle`.`Cliente_ID` = "+ client.Cliente_ID+" AND ( SELECT `IDDispositivo` FROM `datosubicacion` " +
//"AS `Location` WHERE(`Location`.`IDDispositivo` = `Vehicle`.`IdDispositivo` AND " +
//"(`Location`.`Reg_Timestamp` >= '"+ str1+"' AND `Location`.`Reg_Timestamp` <= '"+ str2+"')) LIMIT 1 ) IS NOT NULL LIMIT 1) " + 
//"AS `Vehicle` INNER JOIN `datosubicacion` AS `Locations` ON `identificador_gps` = `Locations`.`IDDispositivo` AND " +
//"(`Locations`.`Reg_Timestamp` >= '"+ str1+"' AND `Locations`.`Reg_Timestamp` <= '"+ str2+"');";
    
//    return models.sequelize.query(q, { model: models.Vehicle })
//  .then(function (resp) {
//        console.log("Si se puede");
//        return resp;
//    // We don't need spread here, since only the results will be returned for select queries
//    }).catch(function (err) {
//        throw Error(err);
//    });
    

    return models.Location.findAndCountAll({
        include: [{
                model: models.Vehicle,
                attributes: [],
                where: {
                    $or: [
                        {
                            Vehiculo_Placas: id
                        }, 
                        {
                            Vehiculo_ID: id
                        }
                    ],
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
            
                }
               
            }],
        attributes: locationDto,
        where: {
            Fecha: {
                $gte: today,
                $lte: tomorrow,
            },
            Reg_Status: 'A'
        },
        offset: parseInt(offset),
        limit: limit
        
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getLastLocation = function (id, client) {
    var today = new Date('2016-01-20');
    today.setUTCHours(0, 0, 0, 0);
    return models.Vehicle.findOne({
        include: [{
                model: models.Location,
                attributes: locationDto,
                where: {
                    Fecha: {
                        $gte: today,
                    },
                    enviado: 0,
                    Reg_Status: 'A'
                   
                },

            }],
        attributes: vehicleDto,
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'         
        },
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            console.log("en then query");
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getLastLocation = function (id, client) {
    var today = new Date('2016-01-20');
    today.setUTCHours(0, 0, 0, 0);
    return models.Vehicle.findOne({
        include: [{
                model: models.Location,
                attributes: locationDto,
                where: {
                    Fecha: {
                        $gte: today,
                    },
                    enviado: 0,
                    Reg_Status: 'A'
                },

            }],
        attributes: vehicleDto,
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
        },
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            console.log("en then query");
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getLastLocation2 = function (id, client) {
    var today = new Date('2016-01-20');
    today.setUTCHours(0, 0, 0, 0);
    return models.Location.findAll({
        include: [{
                model: models.Vehicle,
                attributes: [],
                where: {
                    $or: [
                        {
                            Vehiculo_Placas: id
                        }, 
                        {
                            Vehiculo_ID: id
                        }],
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                },

            }],
        attributes: locationDto,
        where: {
            Fecha: {
                $gte: today,
            },
            enviado: 0,
            Reg_Status: 'A'
        },
        order: [['idDatosLongitud', 'ASC']],
        limit: 1000
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones del vehículo');
        } else {
            console.log("en then query");
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};


exports.updateLocationEnviado = function (ids) {
    return models.Location.update({
        enviado: 1,
    }, {
        where: {
            enviado: 0,
            idDatosLongitud: {
                $in: ids
                                
            }
        }
    }).then(function (resp) {
        return resp;
    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getVehicleRoutes = function (id, client) {
    return models.Vehicle2.findOne({
        include: [{
                model: models.GpsRoutes, 
                attributes: rutaDto,
                where: {
                    reg_status: 'A'
                },
                through: { attributes: [], where: {
                        reg_Status: 'A'
                    } },
                as: 'rutas',
                
            }],
        attributes: [],
        
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron Rutas');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getDriversForVehicle = function (id, client) {
    return models.Vehicle2.findOne({
        include: [{
                model: models.Driver, 
                attributes: driverDto,
                where: {
                    Reg_Status: 'A'
                },
                through: {
                    attributes: [], 
                    where: {
                        Reg_Status: 'A'
                    }
                },
                
            }],
        attributes: [],
        
        where: {
            $or: [
                {
                    Vehiculo_Placas: id
                }, 
                {
                    Vehiculo_ID: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
        },
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron Rutas');
        } else {
            return resp;
        }

    }).catch(function (err) {
        console.log(err);
        throw Error(err);
    });
};