﻿"use strict";
var models = require('../../models');
var vehicleDto = require('../../dto/vehicleDTO');
var error = require('../../Errors/general');
var locationDto = require('../../dto/locationDTO');
var Sequelize = require("sequelize");
var exports = module.exports = {};

exports.findLocationById = function (id, client) {
    return models.Location.findOne({
        where: {
            idDatosLongitud: id,
            Reg_Status: 'A'        
        }, 
        include: [{
                model: models.Vehicle, 
                attributes: vehicleDto,
                where: {
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                }
            }],
        attributes: locationDto,
    }).then(function (resp) {

        if (resp === null) {
            return error.getNotRows('No se encontró la ubicación');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.findAllLocations = function (limit, client) {
    
    
    return models.Location.findAll({
        attributes: locationDto,
        include: [{
                model: models.Vehicle, 
                attributes: vehicleDto,
                where: {
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                }
            }],
        limit: limit,
        where: {
            Reg_Status: 'A'
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.findLocationsLimiting = function (i, limit, client) {

    return models.Location.findAndCountAll({
        attributes: locationDto,
        include: [{
                model: models.Vehicle, 
                attributes: vehicleDto,
               
                where: {
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                }
            }],
        offset: parseInt(i), 
        limit: limit,
        where: {
            Reg_Status: 'A'
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.countRows = function (client) {
    return models.Location.count({

        include: [{
                model: models.Vehicle, 
                attributes: [],
                where: {
                    Cliente_ID: client.Cliente_ID,
                    Reg_Status: 'A'
                }
            }],
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};

exports.getLocationFrom = function (from, to, offset, limit, client) {
    var today = new Date(from);
    today.setUTCHours(0, 0, 0, 0);
    var tomorrow = new Date(to);
    tomorrow.setUTCHours(23, 59, 59, 0);

    return models.Location.findAndCountAll({
        attributes: locationDto,
        include: [{
                model: models.Vehicle, 
                attributes: vehicleDto,
                
                where: {
                    Cliente_ID: client.Cliente_ID
                }
            }],
        where: {
            Fecha: {
                $gte: today,
                $lte: tomorrow
            },
            Reg_Status: 'A'
        },
        offset: parseInt(offset), 
        limit: limit
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron las ubicaciones');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};