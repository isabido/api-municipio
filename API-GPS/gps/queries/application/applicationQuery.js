﻿"use strict";
var models = require('../../models');
var error = require('../../Errors/general');
var _ = require('underscore');
var exports = module.exports = {};

exports.addApplication = function (name, id, secret) {
  
    return models.Application.create({
        name: name,
        id: id,
        clientSecret: secret
    })
    .then(function (resp){
        
        if (resp === null) {
            return error.getNotRows('No se ha dado de alta la aplicación');
        } else {
            return resp;
        }
    })
    .catch(function (err){
        console.log(err);
        var error = {
            message: err.message,
        };
        throw Error(err);
    })
};

