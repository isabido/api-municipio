﻿"use strict";
var models = require('../../models');
var driverDto = require('../../dto/driverDTO');
var error = require('../../Errors/general');
var _ = require('underscore');
var exports = module.exports = {};

exports.findDriverById = function (id, client) {
    return models.Driver.findOne({
        where: {
            $or: [
                {
                    Chofer_ID: id
                }, 
                {
                    Chofer_Numero: id
                }],
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'           
        }
        
    }).then(function (resp) {
        console.log(resp);
        if (resp === null) {
            return error.getNotRows('No se encontró el chofer');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};



exports.findAllDrivers = function (client) {
    return models.Driver.findAll({
        attributes: driverDto,
        where: {
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
        }
    }).then(function (resp) {
       
        if (resp === null) {
            return error.getNotRows('No se encontraron Choferes');
        } else {
            return resp;
        }

    }).catch(function (err) {
  
        throw Error(err);
    });
};

exports.searchDriversFrom = function (from, client) {
    var today = new Date(from);
    today.setUTCHours(0, 0, 0, 0);
    return models.Driver.findAll({
        attributes: driverDto,
        where: {
            Reg_Timestamp: {
                $gte: today,
            },
            Cliente_ID: client.Cliente_ID,
            Reg_Status: 'A'
            
        }
    }).then(function (resp) {
        if (resp === null) {
            return error.getNotRows('No se encontraron choferes');
        } else {
            return resp;
        }

    }).catch(function (err) {
        throw Error(err);
    });
};