﻿var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var methodOverride = require('method-override');
var cors = require('cors');
var logger = require('./log')(module);
var libs = './';
var config = require('./config/config');
//console.log(config);
//logger.info('%s %d %s', config.username, config.password, config.salt);
//logger.log('info', 'test message %s', 'my string');
//log.error('%s ', config.username);


require(libs + 'auth/auth');


var oauth2 = require('./auth/oauth2');

var api = require('./routes/api');
var users = require('./routes/user');
var vehicles = require('./routes/vehicle');
var locations = require('./routes/location');
var gps_routes = require('./routes/gps_route');
var applications = require('./routes/application');
var drivers = require('./routes/driver');

var app = express();
var proxy = require('express-http-proxy');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(methodOverride());
app.use(passport.initialize());

app.use('/', api);
app.use('/api', api);
app.use('/api/v1', api);
app.use('/api/v1/users', users);
app.use('/api/v1/vehicles', vehicles);
app.use('/api/v1/routes', gps_routes);
app.use('/api/v1/drivers', drivers);
app.use('/api/v1/locations', locations);
app.use('/api/v1/applications', applications);
app.use('/api/v1/oauth/token', oauth2.token);
var config = {
    configurable: true,
    value: function () {
        var alt = {};
        var storeKey = function (key) {
            alt[key] = this[key];
        };
        Object.getOwnPropertyNames(this).forEach(storeKey, this);
        return alt;
    }
};
Object.defineProperty(Error.prototype, 'toJSON', config);
// catch 404 and forward to error handler
app.use(function(req, res, next){
    res.status(404);
    //log.debug('%s %d %s', req.method, res.statusCode, req.url);
    res.json({ 
    	message: 'Not found',
        code: 404,
        name: "BadURL"
    });
    return;
});

// error handlers
app.use(function(err, req, res, next){
    res.status(err.status || 500);
    logger.error('%s %d %s', req.method, res.statusCode, err.message);
    console.log(err);
    res.json({
        message: err.message,
        code: 4000,
        name: "QUROGPS-ERROR"
    });
    return;
});

module.exports = app;