﻿module.exports = [['Vehiculo_ID', 'id'], ['Vehiculo_Numero', 'numero'], 
    ['Vehiculo_Marca', 'marca'], ['Vehiculo_Modelo', 'modelo'], ['Vehiculo_Placas', 'placa'],
    ['Reg_Timestamp', 'fecha_registro'], ['Vehiculo_kms', 'kms'], ['Descripcion', 'descripcion'], 
    ['Num_PolizaSeguro', 'numero_poliza'], ['Vencimiento_Poliza', 'vencimiento_poliza'], ['Poliza_Seguro', 'poliza_seguro'],
    ['Tarjeta_Circulacion', 'tarjeta_circulacion'], ['aseguradora', 'aseguradora'], ['IdDispositivo','identificador_gps'], ['Vehiculo_NoEconomico','vehiculo_NoEconomico']];
