﻿module.exports = {
    
    map: {
        name: "",
        clientSecret: "",
        id: 0,
    },
    mappModel: function (model){
        this.map.name = model.application_name,
        this.map.clientSecret = model.application_secret ,
        this.map.id = model.application_id
    },
    getMapModel: function (){
        return this.map;
    }
    
}