﻿module.exports = [['Chofer_ID','id'],['Cliente_ID','id_cliente'], ['Chofer_Numero','numero'], ['Chofer_Nombre','nombre'], ['Chofer_NIP','nip'], 
    ['Chofer_Grupo','grupo'], ['Chofer_Status','estatus'], ['Reg_Status','estatus_baja'], ['Reg_Timestamp','fecha_registro'], ['Chofer_StatusTarjeta','estatus_tarjeta'], 
    ['Correo','correo'], ['Telefono','telefono'], ['Puesto','puesto'], ['Ciudad','ciudad'], ['Numero_Licencia','licencia'], ['Fecha_Vencimiento_Licencia','fecha_vencimiento_licencia'],
    ['Monto_Asignado_Gasolina','monto_gasolina'], ['Digitalizacion_Licencia','licencia'], ['Fotografia','fotografia'], ['Permanente','permanente'],
    ['Chofer_Numero_Web','numero_web'], ['documentos_choferes_id','documentos_id'], ['tipo_licencia','tipo_licencia']];