﻿"use strict";
var libs = process.cwd();
var config = require('../config/config');
var log = require('../log')(module);
var Sequelize = require('sequelize');

module.exports = function () {
    var sequelize = new Sequelize(config.mysql.db_name, config.mysql.username, config.mysql.password, {
        host: config.mysql.host,
        port: config.mysql.port,
        dialect: config.mysql.dialect
    })
        .authenticate()
        .then(function () {
        console.log("conectado...");
        //log.info("Connected to DB!");
    })
        .catch(function (err) {
        console.log("NO SE CONECTOOOO!");
        //log.error('Connection error:', err);
    });
    return sequelize;
}