﻿var crypto = require('crypto');
var config = require('../config/config.json');

module.exports = {
    createSecret: function () {
        var pass = crypto.randomBytes(32).toString('hex');
        var hash = crypto
        .createHmac("sha512", config.salt)
        .update(pass)
        .digest('hex');
        return hash;
    }
};