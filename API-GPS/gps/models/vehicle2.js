﻿"use strict";

var config = require('../config/config.json');
var Promise = require("bluebird");

module.exports = function (sequelize, DataTypes) {
    var Vehicle2 = sequelize.define('Vehicle2', {
        Vehiculo_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        Cliente_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0,
        },
        Autorizacion_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_Numero: {
            type: DataTypes.INTEGER(4).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_Marca: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Vehiculo_Modelo: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        Vehiculo_Placas: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        Vehiculo_NoEconomico: {
            type: DataTypes.STRING(45),
            allowNull: false,
        },
        Vehiculo_UltimaCarga: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: "2001-01-01 00:00:00"
        },
        Vehiculo_UltimoOdometro: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_ControlaRendimiento: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'B'
        },
        Vehiculo_Rendimiento: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_Mas: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_Menos: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_UltimoLts: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_NoTarjeta: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_ControlaOdometro: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        Vehiculo_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'I'
        },
        Vehiculo_Grupo: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        Reg_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        Reg_Timestamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        vehiculo_identificador: {
            type: DataTypes.STRING(100),
            allowNull: false,
            defaultValue: 'SIN PLACA'
        },
        Vehiculo_Checar: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        Vehiculo_RendimientoPromedio: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            defaultValue: 0.00
        },
        Vehiculo_Variacion: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            defaultValue: 0.00
        },
        Vehiculo_kms: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        IdDispositivo: {
            type: DataTypes.STRING(30),
            allowNull: false,
            
            defaultValue: '0'
        },
        IpDispositivo: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: '0'
        },
        Port_gps: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 11751
        },
        Descripcion: {
            type: DataTypes.STRING(300),
            allowNull: false
        },
        Num_PolizaSeguro: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        Vencimiento_Poliza: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: "2001-01-01 00:00:00"
        },
        Poliza_Seguro: {
            type: DataTypes.BLOB,
            allowNull: true
        },
        Tarjeta_Circulacion: {
            type: DataTypes.BLOB,
            allowNull: true
        },
        icono: {
            type: DataTypes.STRING(150),
            allowNull: false
        },
        Tipo_Vehiculo_Gps: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        latitud_: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: '0'
        },
        longitud_: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: '0'
        },
        aseguradora: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: 'N/A'
        }
    },
    {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        // define the table's name
        tableName: 'vehiculo',
        classMethods: {
            associate: function (models) {
                Vehicle2.hasMany(models.Location, {
                    foreignKey: {
                        name: 'IDDispositivo',
                    },
                });
                Vehicle2.belongsToMany(models.GpsRoutes, { as: 'rutas', through: models.RouteVehicle, foreignKey: 'id_vehiculo' });
                Vehicle2.belongsToMany(models.Driver, { through: models.relationshipVehicleDrivers , foreignKey: 'vehiculos_id' });
            }
        }
        
    });
    
    return Vehicle2;
}