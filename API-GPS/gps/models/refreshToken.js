﻿/**
 * Created by IvanIsrael on 02/09/2015.
 */

"use strict"
module.exports = function (sequelize, DataTypes) {
    var RefreshToken = sequelize.define("RefreshToken", {
        token: { type: DataTypes.STRING, unique: true, allowNull: false }
    });
    
    return RefreshToken;
};