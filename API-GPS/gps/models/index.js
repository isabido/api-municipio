﻿"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var env = process.env.NODE_ENV || "development";
var sequelize = require("../db/mysql.js");
var config = require('../config/config.json');
var libs = process.cwd();
var log = require('../log')(module);
var sequelize = new Sequelize(config.db, config.username, config.password, config.mysql);
var db = {};

fs
    .readdirSync(__dirname)
    .filter(function (file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js" && file !== "init.js");
})
    .forEach(function (file) {
    //se importan los modelos que se encuentran en otro archivo
    try {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
        console.log(model.name)
    }
        catch (err) {
        //log.error('Connection error:', err);
            // ^ will output the "unexpected" result of: elsewhere has failed
    }

});

Object.keys(db).forEach(function (modelName) {
    
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;