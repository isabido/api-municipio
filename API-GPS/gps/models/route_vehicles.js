﻿"use strict";

module.exports = function (sequelize, DataTypes) {
    var RouteVehicle = sequelize.define('RouteVehicle', {
        id_relacion: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        id_ruta: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        id_cliente: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        id_vehiculo: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        reg_status: {
            type: DataTypes.STRING(1),
            allowNull: false,
            defaultValue: 'A'
        }


    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        // define the table's name
        tableName: 'relacion_ruta_vehiculos',
        //classMethods: {
        //    associate: function (models) {
        //        //GpsRoutes.belongsTo(models.Vehicle, { foreignKey: 'vehiculo_id', targetKey: 'Vehiculo_ID' });
        //        GpsRoutes.belongsToMany(models.Vehicle, { through: 'UserProject' });
        //    }
        //}
    });

    return RouteVehicle;
}