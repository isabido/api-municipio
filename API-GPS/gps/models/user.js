﻿"use strict";

var crypto = require('crypto');
var config = require('../config/config.json');
var Promise = require("bluebird");

module.exports = function (sequelize, DataTypes) {
    var Cliente_web = sequelize.define('Cliente_web', {
        ClienteWeb_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        Cliente_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        ClienteWeb_Nombre: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        ClienteWeb_Usuario: {
            type: DataTypes.CHAR(12),
            allowNull: false
        },
        ClienteWeb_Pass: {
            type: DataTypes.CHAR(32),
            allowNull: false
        },
        ClienteWeb_Nivel: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        ClienteWeb_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        Reg_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        Reg_TimeStamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        Tiempo_Recargado: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
        },
        limite_velocidad: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: 0
        },
        Perfil_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        tiempo_detenido: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        // define the table's name
        tableName: 'cliente_web',
        classMethods: {
            associate: function (models) {
                Cliente_web.hasMany(models.AccessToken, {
                    onDelete: 'cascade'
                });
                Cliente_web.hasMany(models.RefreshToken, {
                    onDelete: 'cascade'
                });
            },
            encryptPassword: function (password) {
                return crypto.createHash('md5').update(password).digest('hex');
                //crypto.createHmac('sha1', config.salt).update(password).digest('hex');
                    //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
            },
            checkPassword: function (password) {
                return this.encryptPassword(password) === this.password;
            },
        }
    });

    return Cliente_web;
}