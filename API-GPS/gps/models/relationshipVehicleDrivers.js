﻿"use strict";

var config = require('../config/config.json');
var Promise = require("bluebird");

module.exports = function (sequelize, DataTypes) {
    var relationshipVehicleDrivers = sequelize.define('relationshipVehicleDrivers', {
        Relacion_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        choferes_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
        },
        vehiculos_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
        },
        Reg_Status: {
            type: DataTypes.STRING(1),
            allowNull: false,
            defaultValue: 'A'
        },
        
        Reg_TimeStamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        
        Nombre_Chofer: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: 0
        },
        
        ruta_clave: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: 0
        },
        
        vendedores_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        fecha: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: '2001-01-01'
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        tableName: 'relacion_choferes_vehiculos',
             
    });
    
    return relationshipVehicleDrivers;
}
