﻿"use strict";

module.exports = function (sequelize, DataTypes) {
    var Location = sequelize.define('Location', {
        
        idDatosLongitud: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        
        IDDispositivo: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: '0'
        },
        
        Latitud: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: '0'
        },
        
        Longitud: {
            type: DataTypes.STRING(255),
            allowNull: false,
            defaultValue: '0'
        },
        
        Reg_Status: {
            type: DataTypes.CHAR(2),
            allowNull: true,
            defaultValue: 'A'
        },
        
        Reg_TimeStamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        
        IdVehiculo: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        IpDispositivo: {
            type: DataTypes.STRING(20),
            allowNull: true,
        },
        
        NoTransaccion: {
            type: DataTypes.STRING(10),
            allowNull: true
        },
        
        Fecha: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: '0000-00-00 00:00:00'
        },
        
        
        TIPO: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 1
        },
        
        IDGeocerca: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        TipoMotor: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        Direccion: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        
        LongitudConvertida: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: '0'
        },
        
        LatitudConvertida: {
            type: DataTypes.STRING(45),
            allowNull: false,
            defaultValue: '0'
        },
        
        Distancia: {
            type: DataTypes.DECIMAL(20, 4),
            allowNull: true,
            defaultValue: 0.0000
        },
        
        Velocidad: {
            type: DataTypes.DECIMAL(20, 4),
            allowNull: true,
            defaultValue: 0.0000
        },
        
        Fecharecalculo: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: '2001-01-01 00:00:00'
        },
        
        FechaInt: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        Num_Tienda: {
            type: DataTypes.STRING(45),
            allowNull: true,
            defaultValue: '0'
        },
        
        Status_Vehiculo: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        leido: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        id_referencia: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        },
        
        estatusgeocerca: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: 1
        },
        enviado: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        // define the table's name
        tableName: 'datosubicacion',
        classMethods: {
            associate: function (models) {
                Location.belongsTo(models.Vehicle, { foreignKey:'IDDispositivo', targetKey: 'IdDispositivo' });
            }
        } 
             
    });
    
    return Location;
}
