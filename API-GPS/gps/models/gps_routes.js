﻿"use strict";

module.exports = function (sequelize, DataTypes) {
    var GpsRoutes = sequelize.define('GpsRoutes', {
        id_ruta: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        Ruta: {
            type: DataTypes.STRING(150),
            allowNull: false
        },
        reg_status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        reg_timestamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        fecha_inicial: {
            type: DataTypes.DATEONLY,
            allowNull: true,
            defaultValue: '2001-01-01'
        },
        hora_inicio: {
            type: DataTypes.TIME,
            allowNull: true,
            defaultValue: '00:00:00'
        },
        hora_final: {
            type: DataTypes.TIME,
            allowNull: true,
            defaultValue: '00:00:00'
        },
        fecha_final: {
            type: DataTypes.DATEONLY,
            allowNull: true,
            defaultValue: '2001-01-01'
        },
        vehiculo_id: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: true,
            defaultValue: 0
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        // define the table's name
        tableName: 'rutas_gps',
        classMethods: {
            associate: function (models) {
                //GpsRoutes.belongsTo(models.Vehicle, { foreignKey: 'vehiculo_id', targetKey: 'Vehiculo_ID' });
                GpsRoutes.belongsToMany(models.Vehicle2, { as: 'vehiculos', through: models.RouteVehicle, foreignKey: 'id_ruta'});
                //GpsRoutes.belongsToMany(models.Vehicle2, { through: models.RouteVehicle, foreignKey: 'id_ruta', otherKey: 'id_vehiculo' });
            }
        }
    });

    return GpsRoutes;
};