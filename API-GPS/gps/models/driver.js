﻿"use strict";

var config = require('../config/config.json');


module.exports = function (sequelize, DataTypes) {
    var Driver = sequelize.define('Driver', {
        Chofer_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            unique: true
        },
        
        Cliente_ID: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        Chofer_Numero: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 1
        },
        
        Chofer_Nombre: {
            type: DataTypes.STRING(1000),
            allowNull: false
        },
        
        Chofer_NIP: {
            type: DataTypes.CHAR(32),
            allowNull: false
        },
        
        Chofer_Grupo: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        Chofer_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'I'
        },
        
        Reg_Status: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'A'
        },
        
        Reg_TimeStamp: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        
        Chofer_StatusTarjeta: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: 'I'
        },
        
        Correo: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            defaultValue: ''
        },
        Telefono: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            defaultValue: ''
        },
        
        Puesto: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            defaultValue: ''
        },
        
        Ciudad: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            defaultValue: ''
        },
        
        Numero_Licencia: {
            type: DataTypes.STRING(1000),
            allowNull: true,
            defaultValue: ''
        },
        
        Fecha_Vencimiento_Licencia: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: '2001-01-01 00:00:00'
        },
        
        Monto_Asignado_Gasolina: {
            type: DataTypes.STRING(50),
            allowNull: true,
            defaultValue: '0'
        },
        
        Digitalizacion_Licencia: {
            type: DataTypes.BLOB,
            allowNull: true,
            
        },
        
        Fotografia: {
            type: DataTypes.BLOB,
            allowNull: true,
            
        },
        
        Permanente: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        Chofer_Numero_Web: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        documentos_choferes_id: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: 0
        },
        
        tipo_licencia: {
            type: DataTypes.STRING(100),
            allowNull: true,
            defaultValue: 'N/A'
        }

    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        
        
        timestamps: false,
        
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,
        
        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        
        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        
        
        tableName: 'choferes',
        classMethods: {
            associate: function (models) {
                Driver.belongsToMany(models.Vehicle2, { through: models.relationshipVehicleDrivers , foreignKey: 'choferes_id' });
            }
        }
             
    });
    
    return Driver;
}

  
 

